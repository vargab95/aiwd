#include <threads.h>

#include "descriptor.h"

#pragma once

typedef struct {
    process_descriptor_t * descriptor_queue;
    size_t curr_length;
    size_t queue_length;
    mtx_t queue_mtx;
    cnd_t queue_cnd;
} process_queue_t;

typedef enum {
    PQ_ACT_SUCCESS = 0,
    PQ_ACT_FAIL
} pq_action_result_t;

process_queue_t * process_queue_create();
void process_queue_free(process_queue_t * pq);
pq_action_result_t process_queue_add(process_queue_t * pq, process_descriptor_t * desc);
pq_action_result_t process_queue_get(process_queue_t * pq, process_descriptor_t * desc);

extern process_queue_t * g_process_queue;
