#pragma once

typedef enum {
    AIWD_NOT_DEFINED = 0,
    AIWD_OK,
    DG_INVALID_FORMAT,
    DG_NO_TIME,
    RT_CANNOT_CREATE_THRD,
    RT_CANNOT_DETACH_THRD,
    RT_THRD_FAILED,
    PQ_QUEUE_FULL,
    PQ_MEMORY_ERROR,
    PQ_ADD_ERROR
} aiwd_errors_t;
