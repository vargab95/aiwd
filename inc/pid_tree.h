#ifndef __PID_TREE_H__
#define __PID_TREE_H__

#include "descriptor.h"

typedef struct pid_tree_element_t {
    pid_t pid;
    process_descriptor_t desc;

    struct pid_tree_element_t * left;
    struct pid_tree_element_t * right;
} pid_tree_element_t;

typedef struct {
    pid_tree_element_t root;
} pid_tree_t;

pid_tree_t * pt_create();
void pt_destroy(pid_tree_t * tree);
pid_tree_element_t * pt_get(pid_tree_t * tree, pid_t pid);
void pt_add(pid_tree_t * tree, pid_t pid, process_descriptor_t desc);
void pt_remove(pid_tree_t * tree, pid_t pid);

#endif