#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>

#pragma once

typedef enum {
    DEBUG = 0,
    INFO,
    WARNING,
    ERROR,
    CRITICAL
} log_level_t;

typedef struct {
    FILE * output_file;
    uint8_t write_stdout;
    log_level_t report_level;
} logger_t;

logger_t * logger_create(const char * const path);
void logger_destroy(logger_t * logger);
void logger_write(const char * const component_name, const log_level_t lvl, const char * const input_format, ...);