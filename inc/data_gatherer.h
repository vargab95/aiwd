#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "logger.h"
#include "descriptor.h"

typedef struct {
    FILE * fifo_file;
} data_gatherer_t;

data_gatherer_t * data_gatherer_init(char * path);
int data_gatherer_run(void * dgi);
aiwd_errors_t data_gatherer_add(process_descriptor_t * desc);
void data_gatherer_free(data_gatherer_t * dgi);
