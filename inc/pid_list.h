#ifndef __PID_LIST_H__
#define __PID_LIST_H__

#include "descriptor.h"

typedef struct pid_list_element_t {
    process_descriptor_t * desc;
    struct pid_list_element_t * next;
} pid_list_element_t;

typedef struct {
    int length;
    pid_list_element_t * first;
} pid_list_t;

pid_list_t * pl_create();
void pl_destroy(pid_list_t * list);
void pl_add(pid_list_t * list, process_descriptor_t * desc);
void pl_remove(pid_list_t * list, process_descriptor_t * desc);

#endif