#ifndef __PROCESS_MANAGER_H__
#define __PROCESS_MANAGER_H__

#include "descriptor.h"
#include "pid_list.h"
#include "pid_tree.h"

int process_manager_run(void * pmi);
int process_manager_create();

extern pid_tree_t * pid_tree;
extern pid_list_t * pid_list;

#endif