#include <sys/types.h>

#pragma once

typedef enum {
    PS_NOT_USED = 0,
    PS_INITIALIZING,
    PS_RUNNING,
    PS_PREPARE_FOR_ERROR,
    PS_TERMINATION_SENT,
    PS_ENDED
} process_state_t;

typedef struct {
    pid_t pid;
    unsigned int internal_id;
    struct timespec last_signal;
    process_state_t state;
    struct timespec historical[16];
    int lh_id;
    float moving_average;
} process_descriptor_t;
