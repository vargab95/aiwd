#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include "descriptor.h"
#include "process_monitor.h"
#include "process_manager.h"
#include "pid_list.h"
#include "pid_tree.h"
#include "error.h"
#include "logger.h"

static const char * component_name = "process_monitor";

const char * process_state_strs[] = {
    "PS_NOT_USED",
    "PS_INITIALIZING",
    "PS_RUNNING",
    "PS_PREPARE_FOR_ERROR",
    "PS_TERMINATION_SENT",
    "PS_ENDED"
};

int process_monitor_create() {
}

static void update_process_state(process_descriptor_t * ex_desc_ptr) {


}

int process_monitor_run(void * pmi) {
    pid_list_element_t * element;
    process_descriptor_t * desc;
    while(1) {
        logger_write(component_name, DEBUG, "Process monitor is firing");
        element = pid_list->first;
        while (element) {
            desc = element->desc;
            logger_write(component_name, DEBUG, "Processing %d, state %s", desc->pid, process_state_strs[desc->state]);
            switch (desc->state) {
                case PS_NOT_USED:
                    logger_write(component_name, CRITICAL, "Systematic SW failure. PID 0 must not send keep alive signal");
                    break;
                case PS_INITIALIZING:
                    logger_write(component_name, DEBUG, "Waiting for data");
                    break;
                case PS_RUNNING:
                    if ((desc->historical[desc->lh_id].tv_sec - desc->historical[(desc->lh_id == 16) ? (0) : (desc->lh_id + 1)].tv_sec) > (desc->moving_average)) {
                        logger_write(component_name, WARNING, "%d process timeout detected", desc->pid);
                        desc->state = PS_PREPARE_FOR_ERROR;
                    }
                    break;
                case PS_PREPARE_FOR_ERROR:
                    kill (desc->pid, 0);
                    if (ESRCH != errno) {
                        logger_write(component_name, WARNING, "%d: Process is alive, sending termination signal", desc->pid);
                        // kill(desc->pid, SIGTERM);
                    } else {
                        logger_write(component_name, WARNING, "%d: Process is already dead. Go to ended state", desc->pid);
                        desc->state = PS_ENDED;
                    }
                    desc->state = PS_TERMINATION_SENT;
                    break;
                case PS_TERMINATION_SENT:
                    // kill(desc->pid, 0);
                    if (ESRCH != errno) {
                        logger_write(component_name, ERROR, "%d: Process was not terminated. Sending kill signal", desc->pid);
                        // kill(desc->pid, SIGKILL);
                    }
                    desc->state = PS_ENDED;
                    break;
                case PS_ENDED:
                    logger_write(component_name, WARNING, "%d was killed by the AIWD", desc->pid);
                    pl_remove(pid_list, desc);
                    pt_remove(pid_tree, desc->pid);
                    break;
                default:
                    logger_write(component_name, CRITICAL, "Systematic SW failure at updating process descriptor");
                    break;
            }
            element = element->next;
        }
        sleep(1);
    }
}
