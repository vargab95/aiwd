#include <stdlib.h>
#include <limits.h>

#include "pid_tree.h"

#include "logger.h"

static const char component_name[] = "pid_tree";

pid_tree_t * pt_create() {
    pid_tree_t * tree = (pid_tree_t*)malloc(sizeof(pid_tree_t));
    if (!tree) {
        return NULL;
    }

    tree->root.desc = (process_descriptor_t){
        .pid = 0,
        .state = PS_NOT_USED,
        .internal_id = 0
    };
    tree->root.left = NULL;
    tree->root.right = NULL;

    return tree;
}

void pt_destroy(pid_tree_t * tree) {
    // TODO
}

static pid_tree_element_t * pt_get_parent(pid_tree_t * tree, pid_t pid) {
    pid_tree_element_t * element = &tree->root;
    pid_tree_element_t * previous_element = NULL;
    while (1) {
        if (pid == element->desc.pid) {
            return previous_element;
        }
        previous_element = element;
        if (pid < element->desc.pid) {
            if (!element->left) {
                return element;
            }
            element = element->left;
        } else {
            if (!element->right) {
                return element;
            }
            element = element->right;
        }
    }
}

pid_tree_element_t * pt_get(pid_tree_t * tree, pid_t pid) {
    pid_tree_element_t * parent_element = pt_get_parent(tree, pid);
    if (pid < parent_element->desc.pid && parent_element->left) {
        return parent_element->left;
    }
    if (pid >= parent_element->desc.pid && parent_element->right) {
        return parent_element->right;
    }
    return NULL;
}

void pt_add(pid_tree_t * tree, pid_t pid, process_descriptor_t desc) {
    pid_tree_element_t * parent_node = pt_get_parent(tree, pid);
    if (!parent_node) {
        logger_write(component_name, ERROR, "Cannot find proper parent for %d", pid);
        return;
    }

    pid_tree_element_t * new = (pid_tree_element_t*)malloc(sizeof(pid_tree_element_t));

    if (!new) {
        logger_write(component_name, ERROR, "Cannot allocate memory for new element");
        return;
    }

    new->desc = desc;
    new->left = NULL;
    new->right = NULL;

    if (pid < parent_node->desc.pid) {
        parent_node->left = new;
    } else {
        parent_node->right = new;
    }
}

void pt_remove(pid_tree_t * tree, pid_t pid) {
    pid_tree_element_t * parent = pt_get_parent(tree, pid);
    pid_tree_element_t * child;
    if (!parent) {
        logger_write(component_name, CRITICAL, "Non existing element %d found at deletion", pid);
        return;
    }
    logger_write(component_name, DEBUG, "Removing %d from the tree", pid);
    if (pid < parent->desc.pid) {
        child = parent->left;
        parent->left = NULL;
    } else {
        child = parent->right;
        parent->right = NULL;
    }

    if (child->left) {
        pt_add(tree, child->left->desc.pid, child->left->desc);
    }
    if (child->right) {
        pt_add(tree, child->right->desc.pid, child->right->desc);
    }

    // TODO free up removed element
}