#include "pid_list.h"
#include "logger.h"

const char component_name[] = "pid_list";

pid_list_t * pl_create() {
    pid_list_t * list = (pid_list_t *)malloc(sizeof(pid_list_t));
    list->length = 0;
    list->first = 0;
    return list;
}

void pl_destroy(pid_list_t * list) {

}

void pl_add(pid_list_t * list, process_descriptor_t * desc) {
    pid_list_element_t * element = (pid_list_element_t *)malloc(sizeof(pid_list_element_t));
    element->desc = desc;
    element->next = list->first;
    list->first = element;

    logger_write(component_name, DEBUG, "%d was added to the PID list", desc->pid);
}

void pl_remove(pid_list_t * list, process_descriptor_t * desc) {
    pid_list_element_t * element = list->first;
    pid_list_element_t ** prev_element_ptr = &list->first;
    while (element) {
        if (desc == element->desc) {
            logger_write(component_name, DEBUG, "%d was deleted from the PID list", desc->pid);
            *prev_element_ptr = element->next;
            // TODO free(element);
            return;
        }
        prev_element_ptr = &element->next;
        element = element->next;
    }
    logger_write(component_name, WARNING, "%d cannot be found in the PID list. So deletion was failed", desc->pid);
}