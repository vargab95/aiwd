#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdatomic.h>
#include <time.h>
#include <threads.h>

#include <logger.h>

typedef struct {
    const char * component_name;
    time_t time;
    log_level_t lvl;
    char txt[200];
} log_element_t;

static int _write_log_elements(void * attr);

static const char * log_level_to_str[5] = {
    "DBG",
    "INF",
    "WAR",
    "ERR",
    "CRI"
};

static logger_t * logger = NULL;
static atomic_int_least8_t read_idx; // TODO move these to the logger_t
static atomic_int_least8_t write_idx;
static log_element_t logging_queue[128];
static thrd_t thread;
static mtx_t wa_mtx;
static cnd_t wa_cnd;
// TODO: Add mutex to ensure thread safety

logger_t * logger_create(const char * const path) {
    if (logger) return logger;

    logger = (logger_t*)malloc(sizeof(logger_t));
    if (!logger) {
        return NULL;
    }
    
    logger->output_file = NULL;
    logger->write_stdout = 0;
    logger->report_level = DEBUG;
    
    if (path && !(logger->output_file = fopen(path, "a"))) {
        free(logger);
        return NULL;
    }

    if (thrd_success != mtx_init(&wa_mtx, mtx_plain)) {
        if (logger->output_file) fclose(logger->output_file);
        free(logger);
        return NULL;
    }

    if (thrd_success != cnd_init(&wa_cnd)) {
        if (logger->output_file) fclose(logger->output_file);
        mtx_destroy(&wa_mtx);
        free(logger);
        return NULL;
    }

    if (thrd_success != thrd_create(&thread, _write_log_elements, NULL)) {
        if (logger->output_file) fclose(logger->output_file);
        mtx_destroy(&wa_mtx);
        cnd_destroy(&wa_cnd);
        free(logger);
        return NULL;
    }

    return logger;
}

void logger_destroy(logger_t * logger) {
    if (logger->output_file) fclose(logger->output_file);
    mtx_destroy(&wa_mtx);
    cnd_destroy(&wa_cnd);
    free(logger);
}

void logger_write(const char * const component_name, const log_level_t lvl, const char * const input_format, ...) {
    if (lvl >= logger->report_level) {
        va_list args;
        log_element_t * element;
        
        if (((write_idx + 1) % 128) == read_idx) {
            fprintf(stderr, "Log buffer overflow!!! Log message is: %s", input_format);
        } /* else {
            printf("LOGGING WRITE r%d w%d\n", read_idx, write_idx);
        } */
        element = &logging_queue[write_idx];

        time(&element->time);

        element->component_name = component_name;
        element->lvl = lvl;

        va_start (args, input_format);
        vsprintf (element->txt, input_format, args);
        va_end (args);

        write_idx++;
        write_idx %= 128;

        if (thrd_success == mtx_trylock(&wa_mtx)) {
            cnd_signal(&wa_cnd);
            mtx_unlock(&wa_mtx);
        }
    }
}

static int _write_log_elements(void * attr) {
    char buffer[26];
    struct tm* tm_info;
    const char format[] = "[%s] - %15s - %3s: %s\n";
    log_element_t * element;

    if (thrd_success != mtx_lock(&wa_mtx)) {
        return 1;
    }
    while (1) {
        cnd_wait(&wa_cnd, &wa_mtx);
        while (read_idx != write_idx) { 
            // printf("LOGGING READ r%d w%d\n", read_idx, write_idx);
            element = &logging_queue[read_idx++];
            read_idx %= 128;

            tm_info = localtime(&element->time);
            strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);
            if (logger->output_file) {
                fprintf(logger->output_file, format, buffer, element->component_name, log_level_to_str[element->lvl], element->txt);
            }
            if (logger->write_stdout) {
                printf(format, buffer, element->component_name, log_level_to_str[element->lvl], element->txt);
            }
        }
    }
}