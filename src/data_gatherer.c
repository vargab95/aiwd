#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>

#include "data_gatherer.h"
#include "logger.h"
#include "descriptor.h"
#include "process_queue.h"

static const char * component_name = "data_gatherer";

data_gatherer_t * data_gatherer_init(char * path) {
    logger_write(component_name, INFO, "Creating FIFO file");
    mkfifo(path, 0666);
    logger_write(component_name, DEBUG, "Allocating structure");
    data_gatherer_t * dgi = (data_gatherer_t *)malloc(sizeof(data_gatherer_t));
    if (dgi) {
        logger_write(component_name, INFO, "Opening FIFO file");
        if (dgi->fifo_file = fopen(path, "r+")) {
            logger_write(component_name, INFO, "Successfully created");
            return dgi;
        }
        logger_write(component_name, ERROR, "FIFO cannot be opened");
        free(dgi);
    }
    logger_write(component_name, ERROR, "Cannot be created");
    return NULL;
}

int data_gatherer_run(void * dgi) {
    logger_write(component_name, INFO, "Starting data gatherer loop");
    process_descriptor_t desc;

    while(1) {
        if (!fscanf(((data_gatherer_t*)dgi)->fifo_file, "%d,%d", &(desc.pid), &(desc.internal_id))) {
            logger_write(component_name, CRITICAL, "Invalid input format found");
            return DG_INVALID_FORMAT;
        }
        if(clock_gettime(CLOCK_MONOTONIC, &(desc.last_signal))) {
            logger_write(component_name, CRITICAL, "Cannot get time");
            return DG_NO_TIME;
        }
        memset(desc.historical, 0, sizeof(desc.historical));
        desc.lh_id = 0;
        if (PQ_ACT_SUCCESS != process_queue_add(g_process_queue, &desc)) {
            logger_write(component_name, CRITICAL, "Process queue add action was failed");
            return PQ_ADD_ERROR;
        }
        logger_write(component_name, DEBUG, "Iteration performed");
    }
    return AIWD_OK;
}

aiwd_errors_t data_gatherer_add(process_descriptor_t * desc) {
    if (PQ_ACT_SUCCESS != process_queue_add(g_process_queue, desc)) {
        logger_write(component_name, CRITICAL, "Process queue add action was failed");
        return PQ_ADD_ERROR;
    }
    logger_write(component_name, DEBUG, "Process descriptor was successfully added");
    return AIWD_OK;
}

void data_gatherer_free(data_gatherer_t * dgi) {
    logger_write(component_name, INFO, "Freed up");
    fclose(dgi->fifo_file);
    free(dgi);
}
