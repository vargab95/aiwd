#include <stdio.h>
#include <threads.h>
#include "logger.h"
#include "data_gatherer.h"
#include "error.h"
#include "process_queue.h"
#include "process_manager.h"
#include "process_monitor.h"

static const char * component_name = "root";

enum {
    DATA_GATHERER_THREAD = 0,
    PROCESS_MANAGER_THREAD,
    PROCESS_MONITOR_THREAD,
    THREAD_CNT
} thread_names_t;

int main(int argc, char **argv)
{
    int return_code = AIWD_NOT_DEFINED;
    thrd_t processing_threads[THREAD_CNT];
    logger_t * logger = logger_create("aiwd.log");
    logger->write_stdout = 1;
    logger->report_level = DEBUG;
    process_manager_create();
    process_monitor_create();
    g_process_queue = process_queue_create();
    data_gatherer_t * data_gatherer = data_gatherer_init("/tmp/aiwd_fifo");
    if (thrd_success != thrd_create(&(processing_threads[DATA_GATHERER_THREAD]), data_gatherer_run, data_gatherer)) {
        logger_write(component_name, CRITICAL, "Cannot create data gatherer thread");
        return RT_CANNOT_CREATE_THRD;
    }
    logger_write(component_name, DEBUG, "Data gatherer thread was created successfully");
    if (thrd_success != thrd_create(&(processing_threads[PROCESS_MANAGER_THREAD]), process_manager_run, NULL)) {
        logger_write(component_name, CRITICAL, "Cannot create process manager thread");
        return RT_CANNOT_CREATE_THRD;
    }
    logger_write(component_name, DEBUG, "Process monitor thread was created successfully");
    if (thrd_success != thrd_create(&(processing_threads[PROCESS_MONITOR_THREAD]), process_monitor_run, NULL)) {
        logger_write(component_name, CRITICAL, "Cannot create process monitor thread");
        return RT_CANNOT_CREATE_THRD;
    }
    logger_write(component_name, DEBUG, "Process monitor thread was created successfully");
    for (int curr_thrd_id = 0; curr_thrd_id < THREAD_CNT; curr_thrd_id++) {
        if (AIWD_NOT_DEFINED != return_code && AIWD_OK != return_code) {
            logger_write(component_name, INFO, "Thread was finished with error code so kill all other too");
            return return_code;
        }
        else if (thrd_success != thrd_join(processing_threads[curr_thrd_id], &return_code)) {
            logger_write(component_name, CRITICAL, "Thread was returned with error");
            data_gatherer_free(data_gatherer);
            logger_destroy(logger);
            return RT_THRD_FAILED;
        }
        logger_write(component_name, INFO, "Thread was successfully finished");
    }
    data_gatherer_free(data_gatherer);
    process_queue_free(g_process_queue);
    logger_destroy(logger);
    return return_code;
}
