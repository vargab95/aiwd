#include <threads.h>

#include "process_queue.h"
#include "descriptor.h"
#include "logger.h"
#include "error.h"
#include "pid_tree.h"
#include "pid_list.h"

#include "process_manager.h"

static const char * component_name = "process_manager";

pid_tree_t * pid_tree;
pid_list_t * pid_list;

int process_manager_create() {
    pid_tree = pt_create();
    pid_list = pl_create();
}

int process_manager_run(void * pmi) {
    process_descriptor_t desc;
    process_descriptor_t * ex_desc_ptr;
    pid_tree_element_t * element;
    float sum;
    while (1) {
        if (PQ_ACT_SUCCESS != process_queue_get(g_process_queue, &desc)) {
            logger_write(component_name, CRITICAL, "Reading from process queue was failed");
        }
        logger_write(component_name, DEBUG, "Incoming value:");
        logger_write(component_name, DEBUG, "    PID: %d", desc.pid);
        logger_write(component_name, DEBUG, "    Internal ID: %d", desc.internal_id);
        logger_write(component_name, DEBUG, "    Current time: %d", desc.last_signal.tv_sec);
        logger_write(component_name, DEBUG, "    State: %d", desc.state);

        element = pt_get(pid_tree, desc.pid);
        if (element) {
            ex_desc_ptr = &element->desc;
            logger_write(component_name, DEBUG, "Last time: %d", ex_desc_ptr->last_signal.tv_sec);
            if (ex_desc_ptr->state == PS_INITIALIZING && ex_desc_ptr->lh_id == 15) {
                logger_write(component_name, INFO, "%d was initialized", ex_desc_ptr->pid);
                ex_desc_ptr->state = PS_RUNNING;
            }
            ex_desc_ptr->historical[ex_desc_ptr->lh_id].tv_sec = desc.last_signal.tv_sec - ex_desc_ptr->last_signal.tv_sec;            ex_desc_ptr->historical[ex_desc_ptr->lh_id].tv_nsec = desc.last_signal.tv_nsec - ex_desc_ptr->last_signal.tv_nsec;
            ex_desc_ptr->lh_id++;
            ex_desc_ptr->lh_id %= 16;
            ex_desc_ptr->last_signal = desc.last_signal;

            sum = 0;
            for (int i = 0; i < 16; i++) {
                sum += ex_desc_ptr->historical[i].tv_sec;
            }
            ex_desc_ptr->moving_average = sum / 16;
            logger_write(component_name, DEBUG, "New moving average is %.3f", ex_desc_ptr->moving_average);
        } else {
            logger_write(component_name, INFO, "Registering new process (%d)", desc.pid);
            desc.state = PS_INITIALIZING;
            pt_add(pid_tree, desc.pid, desc);
            pl_add(pid_list, &pt_get(pid_tree, desc.pid)->desc);
        }
    }

    return AIWD_OK;
}
