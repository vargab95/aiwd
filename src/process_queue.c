#include <threads.h>

#include "process_queue.h"
#include "logger.h"

process_queue_t * g_process_queue;
static const char * component_name = "process_queue";

process_queue_t * process_queue_create() {
    logger_write(component_name, DEBUG, "Allocating structure");
    process_queue_t * pq = (process_queue_t*)malloc(sizeof(process_queue_t));
    if (!pq) {
        logger_write(component_name, CRITICAL, "Allocating was failed");
        return NULL;
    }
    pq->descriptor_queue = NULL;
    pq->curr_length = 0;
    pq->queue_length = 0;

    if (thrd_success != mtx_init(&(pq->queue_mtx), mtx_plain)) {
        logger_write(component_name, CRITICAL, "Mutex allocation was failed");
        return NULL;
    }
    if (thrd_success != cnd_init(&(pq->queue_cnd))) {
        logger_write(component_name, CRITICAL, "Conditional allocation was failed");
        return NULL;
    }
    if (thrd_success != mtx_lock(&(pq->queue_mtx))) {
        logger_write(component_name, CRITICAL, "Mutex get lock was failed");
        return NULL;
    }
    return pq;
}

void process_queue_free(process_queue_t * pq) {
    cnd_destroy(&(pq->queue_cnd));
    mtx_destroy(&(pq->queue_mtx));
    free(pq->descriptor_queue);
    free(pq);
}

pq_action_result_t process_queue_add(process_queue_t * pq, process_descriptor_t * desc) {
    pq_action_result_t result = PQ_ACT_FAIL;
    if (thrd_success != mtx_lock(&(pq->queue_mtx))) {
        logger_write(component_name, CRITICAL, "Mutex add lock was failed");
        return result;
    }

    if (pq->curr_length >= pq->queue_length) {
        if (pq->queue_length) {
            logger_write(component_name, DEBUG, "Reallocating process queue");
            pq->descriptor_queue = (process_descriptor_t*)realloc(pq->descriptor_queue, (pq->curr_length + 1) * sizeof(process_descriptor_t));
        } else {
            logger_write(component_name, DEBUG, "Allocating process queue");
            pq->descriptor_queue = (process_descriptor_t*)malloc(sizeof(process_descriptor_t));
        }
        if (pq->descriptor_queue) {
            logger_write(component_name, DEBUG, "Process queue length was increased");
            pq->queue_length++;
            result = PQ_ACT_SUCCESS;
        } else {
            logger_write(component_name, CRITICAL, "Allocation of process queue was failed");
        }
    } else {
        result = PQ_ACT_SUCCESS;
    }

    if (PQ_ACT_SUCCESS == result) {
        logger_write(component_name, DEBUG, "Process queue element was set");
        pq->descriptor_queue[pq->curr_length] = *desc;
        pq->curr_length++;
        if (thrd_success != cnd_signal(&(pq->queue_cnd))) {
            logger_write(component_name, CRITICAL, "Conditional signal action was failed");
        }
    }

    if (thrd_success != mtx_unlock(&(pq->queue_mtx))) {
        logger_write(component_name, CRITICAL, "Mutex add unlock was failed");
        result = PQ_ACT_FAIL;
    }

    return result;
}

pq_action_result_t process_queue_get(process_queue_t * pq, process_descriptor_t * desc) {
    if (thrd_success != cnd_wait(&(pq->queue_cnd), &(pq->queue_mtx))) {
        logger_write(component_name, CRITICAL, "Conditional signal action was failed");
        return PQ_ACT_FAIL;
    }

    if (pq->curr_length > 0) {
        pq->curr_length--;
        *desc = pq->descriptor_queue[pq->curr_length];
        return PQ_ACT_SUCCESS;
    }
    
    return PQ_ACT_FAIL;
}
