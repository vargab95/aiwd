# AIWD

Adaptive Iterative Watchdog. A tool to monitor processes which registers into this application.

## Requirements

- It shall support registration through linux pipes.
- It shall monitor the first n run to get the average run time and standard deviation.
- After that it shall monitor these signals and kill if it is not present after x.y time.
- If there is a new information, it should update the stored average time and deviation.

